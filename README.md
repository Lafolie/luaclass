#Lua Class
This is my implmentation of an OOP class object in Lua.

The prototype (parent) class is cloned using a shallow copy, so any table references will need to be carefully set when deriving new classes.